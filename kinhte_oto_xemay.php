<div class="news_box_type2">
    <div class="left_ct">
        <h4 class="title">
            <a href="#">Kinh Tế</a></h4>
            <a class="photo" href="#">
                <img width="340" height="255" src="images/kinhte.png"></a>
                <h5 style="min-height: 46px;">
                    <a class="title_post" href="#">Đặc khu kinh tế Vân Đồn, Phú Quốc sẽ có cơ chế đặc thù</a>
                </h5>
                <ul>
                    <li>
                        <a href="#">Có thể đòi tiền từ Thiên Ngọc Minh Uy như thế nào?</a>
                    </li>
                    <li>
                        <a href="#">Hơn 1.000 ôtô bị thu hồi phù hiệu do vi phạm về truyền dữ liệu</a>
                    </li>
                    <li>
                        <a href="#">Sự thật đáng sợ về loại đũa dùng một lần</a>
                    </li>
                    <li>
                        <a href="#">Đường sắt vừa xin, vừa vay 9.000 tỉ đồng làm gì?</a>
                    </li>
                    <li>
                        <a href="#">ATM ở Sài Gòn ngưng phục vụ sau 22h, Ngân hàng Nhà nước vào cuộc</a>
                    </li>
                </ul>
            </div>
            <div class="right_ct">
                <h4 class="title">
                    <a href="#">Ô Tô - Xe Máy</a></h4>
                    <a class="photo" href="#">
                        <img width="340" height="255" src="images/kinhte.png" alt="Loạt môtô hấp dẫn không cần bằng A2 mới xuất hiện tại VN"></a>
                        <h5 style="min-height: 46px;">
                            <a class="title_post" href="#">Loạt môtô hấp dẫn không cần bằng A2 mới xuất hiện tại VN</a>
                        </h5>
                        <ul>
                            <li>
                                <a href="#">Người Nhật đang thích xe gì của Toyota nhất?</a>
                            </li>
                            <li>
                                <a href="#">SUV siêu sang Mercedes-Maybach G650 Landaulet bị "làm giá" đến 32,1 tỷ Đồng</a>
                            </li>
                            <li>
                                <a href="#">10 mẹo vặt xử lý tình huống hàng ngày trên ôtô</a>
                            </li>
                            <li>
                                <a href="#">Không phải 84 triệu đồng/chiếc, đây là giá mới của những chiếc xe Ấn Độ nhập về VN</a>
                            </li>
                            <li>
                                <a href="#">Ôtô nhập từ Ấn Độ bất ngờ giảm 17 lần</a>
                            </li>
                        </ul>
                    </div>
                    <div class="clrb">
                    </div>
                </div>