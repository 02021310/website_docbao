<div class="news_box_type2">
    <div class="left_ct">
        <h4 class="title">
            <a href="#">Xã Hội</a></h4>
            <a class="photo" href="#">
                <img width="340" height="255" src="images/thegioi_xahoi.jpg" alt="8 người trong nhà cùng làm quan xã chỉ là ngẫu nhiên"></a>
            <h5 style="min-height: 46px;">
                <a class="title_post" href="#">8 người trong nhà cùng làm quan xã chỉ là "ngẫu nhiên"</a></h5>
        <ul>
                <li><a href="#">Vụ đại uý CSGT gây tai nạn ở Hải Dương: “Bệnh con tôi rất nặng và tốn kém…”</a></li>
                <li><a href="#">Dẹp vấn nạn móc nối cho “cò” chăm nuôi bệnh nhân lộng hành</a></li>
                <li><a href="#">Phó phòng thanh tra ở Đà Nẵng đột quỵ trên đường đi làm</a></li>
                <li><a href="#">Tổng cục Du lịch không thay đổi quy hoạch Sơn Trà, Hiệp hội Du lịch Đà Nẵng không ký biên bản cuộc họp</a></li>
                <li><a href="#">Ôtô cháy ở đường làng, xe cứu hỏa đứng ngoài bất lực</a></li>
        </ul>
    </div>
    <div class="right_ct">
        <h4 class="title">
            <a href="/tin-tuc/the-gioi/35">Thế Giới</a></h4>
            <a class="photo" href="/tin-tuc/11-05-2017/be-gai-6-thang-tuoi-bi-cho-pit-bull-nuoi-9-nam-can-chet-khi-dang-choi-trong-xe-tap-di/35/452047">
                <img width="340" height="255" src="images/thegioi_xahoi.jpg" alt="Bé gái 6 tháng tuổi bị chó Pit bull nuôi 9 năm cắn chết khi đang chơi trong xe tập đi"></a>
            <h5 style="min-height: 46px;">
                <a class="title_post" href="/tin-tuc/11-05-2017/be-gai-6-thang-tuoi-bi-cho-pit-bull-nuoi-9-nam-can-chet-khi-dang-choi-trong-xe-tap-di/35/452047">Bé gái 6 tháng tuổi bị chó Pit bull nuôi 9 năm cắn chết khi đang chơi trong xe tập đi</a></h5>
        <ul>
                <li><a href="/tin-tuc/11-05-2017/trieu-tien-muon-dan-do-nguoi-am-muu-am-sat-kim-jong-un/35/452030">Triều Tiên muốn dẫn độ người âm mưu ám sát Kim Jong Un</a></li>
                <li><a href="/tin-tuc/11-05-2017/tan-tong-thong-han-quoc-tung-de-vo-cau-hon-truoc-dam-dong/35/452028">Tân tổng thống Hàn Quốc từng để vợ cầu hôn trước đám đông</a></li>
                <li><a href="/tin-tuc/11-05-2017/chu-tich-nuoc-va-phu-nhan-den-bac-kinh/35/452019">Chủ tịch nước và phu nhân đến Bắc Kinh</a></li>
                <li><a href="/tin-tuc/11-05-2017/my-nga-tan-cong-mang-can-thiep-bau-cu-phap/35/452002">Mỹ: Nga tấn công mạng can thiệp bầu cử Pháp</a></li>
                <li><a href="/tin-tuc/11-05-2017/tong-thong-han-quoc-noi-chuyen-voi-ong-tap-va-ong-abe-ve-trieu-tien/35/451999">Tổng thống Hàn Quốc nói chuyện với ông Tập và ông Abe về Triều Tiên</a></li>
        </ul>
    </div>
    <div class="clrb">
    </div>
</div>