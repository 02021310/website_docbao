﻿
function loadTime() {
    var day_of_week = new Array();
    day_of_week[0] = "Chủ Nhật";
    day_of_week[1] = "Thứ Hai";
    day_of_week[2] = "Thứ Ba";
    day_of_week[3] = "Thứ Tư";
    day_of_week[4] = "Thứ Năm";
    day_of_week[5] = "Thứ Sáu";
    day_of_week[6] = "Thứ Bảy";

    var month_of_year = new Array();
    month_of_year[0] = "Tháng 1";
    month_of_year[1] = "Tháng 2";
    month_of_year[2] = "Tháng 3";
    month_of_year[3] = "Tháng 4";
    month_of_year[4] = "Tháng 5";
    month_of_year[5] = "Tháng 6";
    month_of_year[6] = "Tháng 7";
    month_of_year[7] = "Tháng 8";
    month_of_year[8] = "Tháng 9";
    month_of_year[9] = "Tháng 10";
    month_of_year[10] = "Tháng 11";
    month_of_year[11] = "Tháng 12";

    var nowdate = new Date();
    var curyear = nowdate.getYear();
    var curmonth = nowdate.getMonth();
    var curdate = nowdate.getDate();
    var curday = nowdate.getDay();

    var curhour = nowdate.getHours();
    var curminute = nowdate.getMinutes();
    var cursecond = nowdate.getSeconds();

    if (curyear < 1000) { curyear = 1900 + curyear; }
    if (curdate < 10) { curdate = "0" + curdate; }

    $('#timeNow').html(day_of_week[curday] + ", " + curdate + "/" + (curmonth + 1) + "/" + curyear + " | " + curhour + ":" + curminute + " GMT+7");
}