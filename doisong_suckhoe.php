<div class="news_box_type2">
    <div class="left_ct">
        <h4 class="title">
            <a href="/tin-tuc/doi-song/13">Đời sống</a></h4>
            <a class="photo" href="/tin-tuc/11-05-2017/tam-quen-me-chong-kho-tinh-di-chi-em-co-gai-nay-duoc-nha-chong-cung-hon-ca-khi-o-nha-de-nua/13/452046">
                <img width="340" height="255" src="images/doisong.jpg"></a>
            <h5 style="min-height: 46px;">
                <a class="title_post" href="/tin-tuc/11-05-2017/tam-quen-me-chong-kho-tinh-di-chi-em-co-gai-nay-duoc-nha-chong-cung-hon-ca-khi-o-nha-de-nua/13/452046">Tạm quên mẹ chồng khó tính đi chị em, cô gái này được nhà chồng cưng hơn cả khi ở nhà đẻ nữa!</a></h5>
        <ul>
                <li><a href="/tin-tuc/11-05-2017/chuyen-cam-dong-sau-buc-anh-ong-bo-dua-bup-be-di-shopping/13/452001">Chuyện cảm động sau bức ảnh ông bố đưa búp bê đi shopping</a></li>
                <li><a href="/tin-tuc/11-05-2017/thoi-quen-sai-lam-cua-hang-nghin-me-viet-khien-con-nho-de-phai-nhap-vien-khan-cap/13/451991">Thói quen sai lầm của hàng nghìn mẹ Việt khiến con nhỏ dễ phải nhập viện khẩn cấp</a></li>
                <li><a href="/tin-tuc/11-05-2017/3-con-giap-can-doi-ket-to-hon-nua-trong-6-thang-cuoi-nam-2017/13/451975">3 con giáp cần đổi KÉT to hơn nữa trong 6 tháng cuối năm 2017 </a></li>
                <li><a href="/tin-tuc/11-05-2017/bo-cang-gan-gui-tre-cang-thong-minh/13/451958">Bố càng gần gũi, trẻ càng thông minh</a></li>
                <li><a href="/tin-tuc/11-05-2017/cong-nghe-lam-dep-rung-minh-cua-chi-em-thoi-bao-cap/13/451906">Công nghệ làm đẹp rùng mình của chị em thời bao cấp</a></li>
        </ul>
    </div>
    <div class="right_ct">
        <h4 class="title">
            <a href="/tin-tuc/suc-khoe/16">Sức Khỏe</a></h4>
            <a class="photo" href="/tin-tuc/11-05-2017/sai-lam-lon-khi-lam-chuyen-ay-cua-rat-nhieu-cap-doi-tre-tuoi/16/451967">
                <img width="340" height="255" src="images/doisong.jpg" alt="Sai lầm lớn khi làm &quot;chuyện ấy&quot; của rất nhiều cặp đôi trẻ tuổi"></a>
            <h5 style="min-height: 46px;">
                <a class="title_post" href="/tin-tuc/11-05-2017/sai-lam-lon-khi-lam-chuyen-ay-cua-rat-nhieu-cap-doi-tre-tuoi/16/451967">Sai lầm lớn khi làm "chuyện ấy" của rất nhiều cặp đôi trẻ tuổi</a></h5>
        <ul>
                <li><a href="/tin-tuc/11-05-2017/phu-nu-len-dinh-trong-chuyen-ay-va-mot-loi-ich-khong-ai-nghi-den/16/451963">Phụ nữ "lên đỉnh" trong "chuyện ấy" và một lợi ích không ai nghĩ đến</a></li>
                <li><a href="/tin-tuc/11-05-2017/300-trieu-tinh-trung-boi-dua-den-gap-trung-duoi-goc-nhin-hai-huoc/16/451921">300 triệu tinh trùng bơi đua đến gặp trứng dưới góc nhìn hài hước</a></li>
                <li><a href="/tin-tuc/11-05-2017/dau-tinh-hoan-sau-khi-thu-dam/16/451850">Đau tinh hoàn sau khi thủ dâm</a></li>
                <li><a href="/tin-tuc/11-05-2017/bao-ngu-tang-sinh-luc-quy-ong/16/451836">Bào ngư tăng sinh lực quý ông</a></li>
                <li><a href="/tin-tuc/11-05-2017/nhung-bi-kip-nam-long-cho-lan-dau-lam-chuyen-ay/16/451812">Những bí kíp nằm lòng cho lần đầu làm chuyện ấy</a></li>
        </ul>
    </div>
    <div class="clrb">
    </div>
</div>