function(b) {
  b = b || windowAlias.event;
  var c = getNameOfClickedButton(b),
    d = getTargetElementFromEvent(b);
  if ("click" === b.type) {
    var e = !1;
    a && "middle" === c && (e = !0), d && !e && processClick(d)
  } else "mousedown" === b.type ? "middle" === c && d ? (lastButton = c, lastTarget = d) : lastButton = lastTarget = null : "mouseup" === b.type ? (c === lastButton && d === lastTarget && processClick(d), lastButton = lastTarget = null) : "contextmenu" === b.type && processClick(d)
}
