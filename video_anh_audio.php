<div class=" clip">
    <div class="ovh bar">
        <a href="#">VIDEO / ẢNH / AUDIO</a>
        <div class="slide_clip_page" data-jcarouselpagination="true">
            <a href="javascript:void(0)">1</a>
            <a href="javascript:void(0)">2</a>
            <a href="javascript:void(0)" class="active">3</a>
        </div>
    </div>
    
    <div class="jcarousel slide_clip" data-jcarousel="true" data-jcarouselautoscroll="true">
        <ul style="left: -1408px; top: 0px;">
                <li><a href="#">
                    <img width="156" src="images/imgaudio.jpg">
                    <h3>Hình ảnh Thủ tướng bắt đầu các hoạt động tại WEF-ASEAN</h3>

                    <i style="display:none" class="sprite video_ico"></i>                    
                    </a>
                </li>

                <li><a href="#">
                    <img width="156" src="images/imgaudio.jpg" alt="Hình ảnh Chủ tịch nước Trần Đại Quang bắt đầu chuyến thăm Trung Quốc">
                    <h3>Hình ảnh Chủ tịch nước Trần Đại Quang bắt đầu chuyến thăm Trung Quốc</h3>

                    <i style="display:none" class="sprite video_ico">
                        
                    </i>                    
                    </a>
                </li>

                <li><a href="#" title="Loạt môtô hấp dẫn không cần bằng A2 mới xuất hiện tại VN">
                    <img width="156" src="images/imgaudio.jpg" alt="Loạt môtô hấp dẫn không cần bằng A2 mới xuất hiện tại VN">
                    <h3>Loạt môtô hấp dẫn không cần bằng A2 mới xuất hiện tại VN</h3>

                    <i style="display:none" class="sprite video_ico"></i>                    
                    </a>
                </li>

                <li><a href="#" title="Ăn khoai lang có thể giúp giảm cân">
                    <img width="156" src="images/imgaudio.jpg" alt="Ăn khoai lang có thể giúp giảm cân">
                    <h3>Ăn khoai lang có thể giúp giảm cân</h3>

                    <i style="display:none" class="sprite video_ico"></i>                    
                    </a>
                </li>

                <li><a href="#" title="Toàn cảnh nhà ga quốc tế Đà Nẵng 3.500 tỷ sắp khánh thành">
                    <img width="156" src="images/imgaudio.jpg" alt="Toàn cảnh nhà ga quốc tế Đà Nẵng 3.500 tỷ sắp khánh thành">
                    <h3>Toàn cảnh nhà ga quốc tế Đà Nẵng 3.500 tỷ sắp khánh thành</h3>

                    <i style="display:none" class="sprite video_ico"></i>                    
                    </a>
                </li>
                

                <li><a href="#" title="Video: Đường đi lòng vòng của một tấn thuốc tây lậu trị giá 5 tỷ">
                    <img width="156" src="images/imgaudio.jpg" alt="Video: Đường đi lòng vòng của một tấn thuốc tây lậu trị giá 5 tỷ">
                    <h3>Video: Đường đi lòng vòng của một tấn thuốc tây lậu trị giá 5 tỷ</h3>

                    <i style="display:none" class="sprite video_ico"></i>                    
                    </a>
                </li>


                <li><a href="#" title="Nước sinh hoạt đóng cặn như gạch cua ở khu dân cư Hà Nội">
                    <img width="156" src="images/imgaudio.jpg" alt="Nước sinh hoạt đóng cặn như gạch cua ở khu dân cư Hà Nội">
                    <h3>Nước sinh hoạt đóng cặn như gạch cua ở khu dân cư Hà Nội</h3>

                    <i style="display:none" class="sprite video_ico">
                        
                    </i>                    
                        </a>
                </li>


                <li><a href="#" title="13 tuyến đường trên thế giới sẽ khiến bạn muốn thót tim khi đi qua">
                    <img width="156" src="images/imgaudio.jpg" alt="13 tuyến đường trên thế giới sẽ khiến bạn muốn thót tim khi đi qua">
                    <h3>13 tuyến đường trên thế giới sẽ khiến bạn muốn thót tim khi đi qua</h3>

                         <i style="display:none" class="sprite video_ico"></i>                    
                      </a>

                </li>


                <li><a href="#" title="Soi “biệt thự tuyệt nhất thế giới” của Tổng thống Donald Trump">
                    <img width="156" src="images/imgaudio.jpg" alt="Soi “biệt thự tuyệt nhất thế giới” của Tổng thống Donald Trump">
                    <h3>Soi “biệt thự tuyệt nhất thế giới” của Tổng thống Donald Trump</h3>

                        <i style="display:none" class="sprite video_ico"></i>                    
                    </a>
                </li>

                <li><a href="#" title="Video: Đêm đâm chém loạn xạ trong shop thời trang của toán thanh niên">
                    <img width="156" src="images/imgaudio.jpg" alt="Video: Đêm đâm chém loạn xạ trong shop thời trang của toán thanh niên">
                    <h3>Video: Đêm đâm chém loạn xạ trong shop thời trang của toán thanh niên</h3>

                        <i style="display:none" class="sprite video_ico"></i>                   
                        </a>
                </li>


                <li><a href="#" title="Video: Bùng binh “ma trận” tại ngã ba Ninh Hòa">
                    <img width="156" src="images/imgaudio.jpg" alt="Video: Bùng binh “ma trận” tại ngã ba Ninh Hòa">
                    <h3>Video: Bùng binh “ma trận” tại ngã ba Ninh Hòa</h3>

                        <i style="display:none" class="sprite video_ico"></i>                   
                        </a>
                </li>


                <li><a href="#" title="Video: 5 nút giao thông nhiều tầng xe chạy ở Hà Nội">
                    <img width="156" src="images/imgaudio.jpg" alt="Video: 5 nút giao thông nhiều tầng xe chạy ở Hà Nội">
                    <h3>Video: 5 nút giao thông nhiều tầng xe chạy ở Hà Nội</h3>

            <i style="display:none" class="sprite video_ico"></i>                    
                </a></li>
        </ul>
    </div>
</div>