<div class="news_top">
    <div class="news_top_hd">
        <span class="time"></span>
        <div class="news_top_tabs">
            <a id="news-top-tabs-1" href="javascript:void(0);" class="">Tin nóng</a> <a style="background-color: #01BBF2; color: #ffffff" id="news-top-tabs-2" href="javascript:void(0);" class="on">Sự kiện nổi bật;
                <img src="images/icon_hot_1.gif" height="10px"></a>
        </div>
    </div>
    <div class="news_top_ct" id="news-top-ct-1" style="display: none;">
    <div class="ct_left">
    	<div id="itm-news0" class="news_item" style="display: none;">                
    		<a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/loat-tin-nhan-ga-gam-trai-la-nghi-cua-minh-beo-khien-cu-dan-mang-day-song-20-11052017235554-400x300.jpg" alt="Loạt tin nhắn gạ gẫm trai lạ nghi của Minh Béo khiến cư dân mạng dậy sóng">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="/tin-tuc/11-05-2017/loat-tin-nhan-ga-gam-trai-la-nghi-cua-minh-beo-khien-cu-dan-mang-day-song/20/452065">Loạt tin nhắn gạ gẫm trai lạ nghi của Minh Béo khiến cư dân mạng dậy sóng</a></h4>
                        <p>Sau đoạn clip xin lỗi, Minh Béo bị cho là lại 'chứng nào tật nấy' khi có ý định gạ gẫm một chàng trai trên facebook.</p>
                    </div>
    	</div>
		<div id="itm-news1" class="news_item" style="display: none;">                
			<a class="photo" href="/tin-tuc/11-05-2017/my-tam-tra-loi-ve-tin-lam-dam-cuoi-vao-235/20/452059">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/my-tam-tra-loi-ve-tin-lam-dam-cuoi-vao-235-20-11052017231217-400x300.jpg" alt="Mỹ Tâm trả lời về tin làm đám cưới vào 23/5">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/my-tam-tra-loi-ve-tin-lam-dam-cuoi-vao-235/20/452059">Mỹ Tâm trả lời về tin làm đám cưới vào 23/5</a></h4>
                    <p>'Họa mi tóc nâu' lần đầu lên tiếng về thông tin cô sắp cưới chồng đang gây xôn xao showbiz Việt. </p>
                </div>
	    </div>
		  <div id="itm-news2" class="news_item" style="display: none;">                
			<a class="photo" href="/tin-tuc/11-05-2017/ong-bui-cao-nhat-quan-tu-chuc-pho-chu-tich-novaland/30/452051">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/ong-bui-cao-nhat-quan-tu-chuc-pho-chu-tich-novaland-30-11052017220701-400x300.jpg" alt="Ông Bùi Cao Nhật Quân từ chức Phó chủ tịch Novaland">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/ong-bui-cao-nhat-quan-tu-chuc-pho-chu-tich-novaland/30/452051">Ông Bùi Cao Nhật Quân từ chức Phó chủ tịch Novaland</a></h4>
                    <p>HĐQT CTCP Tập đoàn Đầu tư Địa ốc No Va (Novaland) vừa thông qua đơn từ nhiệm của Phó chủ tịch Bùi Cao Nhật Quân.</p>
                </div>
	    </div>
		<div id="itm-news3" class="news_item" style="display: none;">               
			<a class="photo" href="/tin-tuc/11-05-2017/den-ca-chuyen-di-an-voi-dong-nghiep-me-chong-cung-quan-thuc-nang-dau-chat-che/31/452048">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/den-ca-chuyen-di-an-voi-dong-nghiep-me-chong-cung-quan-thuc-nang-dau-chat-che-31-11052017214347-400x300.jpg" alt="Đến cả chuyện đi ăn với đồng nghiệp, mẹ chồng cũng quản thúc nàng dâu chặt chẽ">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/den-ca-chuyen-di-an-voi-dong-nghiep-me-chong-cung-quan-thuc-nang-dau-chat-che/31/452048">Đến cả chuyện đi ăn với đồng nghiệp, mẹ chồng cũng quản thúc nàng dâu chặt chẽ</a></h4>
                    <p>"Tốt nhất không nên đi, chồng đi công tác xa, đàn bà phụ nữ đêm hôm ra đường cũng chẳng hay ho gì" - bà Phương của "Sống chung với mẹ chồng" răn đe con dâu.</p>
                </div>
	   </div>
		<div id="itm-news4" class="news_item" style="display: none;">                
			<a class="photo" href="/tin-tuc/11-05-2017/vu-3-anh-em-ruot-tu-vong-o-hai-duong-nguoi-than-khoc-ngat-khi-biet-tin/29/452043">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/vu-3-anh-em-ruot-tu-vong-o-hai-duong-nguoi-than-khoc-ngat-khi-biet-tin-29-11052017205431-400x300.jpg" alt="Vụ 3 anh em ruột tử vong ở Hải Dương: Người thân khóc ngất khi biết tin">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/vu-3-anh-em-ruot-tu-vong-o-hai-duong-nguoi-than-khoc-ngat-khi-biet-tin/29/452043">Vụ 3 anh em ruột tử vong ở Hải Dương: Người thân khóc ngất khi biết tin</a></h4>
                    <p>“Sang đến nơi thấy cảnh tượng như vậy tôi không còn đứng vững được nữa, đầu óc choáng váng và bị sốc....", bà Thể cho biết.</p>
                </div>
	   </div>
		<div id="itm-news5" class="news_item" style="display: none;">                
			<a class="photo" href="/tin-tuc/11-05-2017/csgt-len-tieng-ve-vu-dung-tay-va-cham-khien-nguoi-vi-pham-do-mau/33/452040">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/csgt-len-tieng-ve-vu-dung-tay-va-cham-khien-nguoi-vi-pham-do-mau-33-11052017202206-400x300.jpg" alt="CSGT lên tiếng về vụ dùng tay “va chạm” khiến người vi phạm đổ máu">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/csgt-len-tieng-ve-vu-dung-tay-va-cham-khien-nguoi-vi-pham-do-mau/33/452040">CSGT lên tiếng về vụ dùng tay “va chạm” khiến người vi phạm đổ máu</a></h4>
                    <p>Sau khi đoạn clip được mạng xã hội chia sẻ ghi nhận cảnh nam thanh niên miệng chảy máu được lan truyền, Phòng CSGT đường bộ - đường sắt PC67, Công an TP.HCM đã lên tiếng.</p>
                </div>
	   </div>
		<div id="itm-news6" class="news_item" style="display: none;">                
			<a class="photo" href="/tin-tuc/11-05-2017/chan-dong-chong-danh-chet-vo-giau-xac-suot-4-nam-o-ho-ga-trong-nha/33/452037">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/chan-dong-chong-danh-chet-vo-giau-xac-suot-4-nam-o-ho-ga-trong-nha-33-11052017195634-400x300.jpg" alt="CHẤN ĐỘNG: Chồng đánh chết vợ, giấu xác suốt 4 năm ở hố ga trong nhà">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/chan-dong-chong-danh-chet-vo-giau-xac-suot-4-nam-o-ho-ga-trong-nha/33/452037">CHẤN ĐỘNG: Chồng đánh chết vợ, giấu xác suốt 4 năm ở hố ga trong nhà</a></h4>
                    <p>Trong quá trình mâu thuẫn vì nghi vợ cặp bồ, Hoàng lấy khúc củi khô đánh chết vợ. Sau đó, người chồng đã giấu xác vợ suốt 4 năm ở hố ga trong nhà.</p>
                </div>
	   </div>
		<div id="itm-news7" class="news_item" style="display: none;">                
			<a class="photo" href="/tin-tuc/11-05-2017/trung-quoc-ban-21-loat-dai-bac-chao-mung-chu-tich-nuoc-tran-dai-quang/35/452033">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/trung-quoc-ban-21-loat-dai-bac-chao-mung-chu-tich-nuoc-tran-dai-quang-35-11052017193139-400x300.jpg" alt="Trung Quốc bắn 21 loạt đại bác chào mừng Chủ tịch nước Trần Đại Quang">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/trung-quoc-ban-21-loat-dai-bac-chao-mung-chu-tich-nuoc-tran-dai-quang/35/452033">Trung Quốc bắn 21 loạt đại bác chào mừng Chủ tịch nước Trần Đại Quang</a></h4>
                    <p>Chiều nay, lễ đón chính thức Chủ tịch nước Trần Đại Quang và phu nhân thăm cấp nhà nước tới TQ đã được tổ chức tại Đại lễ đường Nhân dân, Bắc Kinh theo nghi thức trọng thể dành đón nguyên thủ quốc gia.</p>
                </div>
	   </div>
		<div id="itm-news8" class="news_item" style="display: none;">                
			<a class="photo" href="/tin-tuc/11-05-2017/xe-hop-lan-lan-phi-thang-xe-khach-tren-cao-toc-ha-noi---lao-cai/29/452023">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/xe-hop-lan-lan-phi-thang-xe-khach-tren-cao-toc-ha-noi---lao-cai-33-11052017174330-400x300.jpg" alt="Xế hộp lấn làn phi thẳng xe khách trên cao tốc Hà Nội - Lào Cai">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/xe-hop-lan-lan-phi-thang-xe-khach-tren-cao-toc-ha-noi---lao-cai/29/452023">Xế hộp lấn làn phi thẳng xe khách trên cao tốc Hà Nội - Lào Cai</a></h4>
                    <p>Vụ tai nạn xảy ra vào khoảng 15h15 chiều nay trên cao tốc Nội Bài - Lào Cai khiến nhiều người bị thương.</p>
                </div>
	   </div>
		<div id="itm-news9" class="news_item">                
			<a class="photo" href="/tin-tuc/11-05-2017/bac-si-cho-thai-phu-ngung-tim-cuu-2-me-con-thoat-chet/14/452022">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/bac-si-cho-thai-phu-ngung-tim-cuu-2-me-con-thoat-chet-14-11052017173722-400x300.jpg" alt="Bác sĩ cho thai phụ ngưng tim, cứu 2 mẹ con thoát chết">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="/tin-tuc/11-05-2017/bac-si-cho-thai-phu-ngung-tim-cuu-2-me-con-thoat-chet/14/452022">Bác sĩ cho thai phụ ngưng tim, cứu 2 mẹ con thoát chết</a></h4>
                    <p>Khi thai nhi được 12 tuần, thai phụ 20 tuổi người Bình Dương bất ngờ rơi vào tình trạng lơ mơ, mạch và huyết áp không ổn định, phải dùng thuốc hỗ trợ.</p>
                </div>
		</div> 
    </div>
        <div class="ct_right">
            <ul class="news_lst">
                    <li>
                    	<a onmouseover="Slidenews(0)" href="#">Loạt tin nhắn gạ gẫm trai lạ nghi của Minh Béo khiến cư dân mạng dậy sóng</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(1)" href="#">Mỹ Tâm trả lời về tin làm đám cưới vào 23/5</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(2)" href="#">Ông Bùi Cao Nhật Quân từ chức Phó chủ tịch Novaland</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(3)" href="#">Đến cả chuyện đi ăn với đồng nghiệp, mẹ chồng cũng quản thúc nàng dâu chặt chẽ</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(4)" href="#">Vụ 3 anh em ruột tử vong ở Hải Dương: Người thân khóc ngất khi biết tin</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(5)" href="#">CSGT lên tiếng về vụ dùng tay “va chạm” khiến người vi phạm đổ máu</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(6)" href="#">CHẤN ĐỘNG: Chồng đánh chết vợ, giấu xác suốt 4 năm ở hố ga trong nhà</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(7)" href="#">Trung Quốc bắn 21 loạt đại bác chào mừng Chủ tịch nước Trần Đại Quang</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(8)" href="#">Xế hộp lấn làn phi thẳng xe khách trên cao tốc Hà Nội - Lào Cai</a>
                    </li>
                    <li>
                    	<a onmouseover="Slidenews(9)" href="#">Bác sĩ cho thai phụ ngưng tim, cứu 2 mẹ con thoát chết</a>
                    </li>

                    <div style="font-weight: bold; line-height: 35px; display:none">Tin doanh nghiệp</div>
                        <li style="display:none"><a href="#">FE CREDIT ký kết hợp đồng hợp vốn 100 triệu USD với ngân hàng Credit Suisse</a></li>
                   </ul>

        </div>
        <div class="clrb">
        </div>

    </div>

  