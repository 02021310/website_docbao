<div class="news_top">
    <div class="news_top_hd">
        <span class="time"></span>
        <div class="news_top_tabs">
            <a id="news-top-tabs-1" href="javascript:void(0);" class="">Tin nổi bật</a>
            <a style="background-color: #01BBF2; color: #ffffff" id="news-top-tabs-2" href="javascript:void(0);" class="on">Sự kiện nổi bật;
                <img src="images/icon_hot_1.gif" height="10px"></a>
        </div>
    </div>
    <div class="news_top_ct" id="news-top-ct-1" style="display: none;">
        <div class="ct_left">
            <div id="itm-news0" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                            <div class="crop">
                                <img width="400" src="images/sukiennoibat.png" alt="Loạt tin nhắn gạ gẫm trai lạ nghi của Minh Béo khiến cư dân mạng dậy sóng">
                            </div>
                        </a>
                        <div class="text">
                            <h4>
                                <a href="images/sukiennoibat.png">Loạt tin nhắn gạ gẫm trai lạ nghi của Minh Béo khiến cư dân mạng dậy sóng</a></h4>
                            <p>Sau đoạn clip xin lỗi, Minh Béo bị cho là lại 'chứng nào tật nấy' khi có ý định gạ gẫm một chàng trai trên facebook.</p>
                        </div>
            </div>
            <div id="itm-news1" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="Mỹ Tâm trả lời về tin làm đám cưới vào 23/5">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">Mỹ Tâm trả lời về tin làm đám cưới vào 23/5</a></h4>
                        <p>'Họa mi tóc nâu' lần đầu lên tiếng về thông tin cô sắp cưới chồng đang gây xôn xao showbiz Việt. </p>
                    </div>
            </div>
              <div id="itm-news2" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <s width="400" src="images/sukiennoibat.png">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">Ông Bùi Cao Nhật Quân từ chức Phó chủ tịch Novaland</a></h4>
                        <p>HĐQT CTCP Tập đoàn Đầu tư Địa ốc No Va (Novaland) vừa thông qua đơn từ nhiệm của Phó chủ tịch Bùi Cao Nhật Quân.</p>
                    </div>
            </div>
            <div id="itm-news3" class="news_item" style="display: none;">               
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="Đến cả chuyện đi ăn với đồng nghiệp, mẹ chồng cũng quản thúc nàng dâu chặt chẽ">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">Đến cả chuyện đi ăn với đồng nghiệp, mẹ chồng cũng quản thúc nàng dâu chặt chẽ</a></h4>
                        <p>"Tốt nhất không nên đi, chồng đi công tác xa, đàn bà phụ nữ đêm hôm ra đường cũng chẳng hay ho gì" - bà Phương của "Sống chung với mẹ chồng" răn đe con dâu.</p>
                    </div>
           </div>
            <div id="itm-news4" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="Vụ 3 anh em ruột tử vong ở Hải Dương: Người thân khóc ngất khi biết tin">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="/tin-tuc/11-05-2017/vu-3-anh-em-ruot-tu-vong-o-hai-duong-nguoi-than-khoc-ngat-khi-biet-tin/29/452043">Vụ 3 anh em ruột tử vong ở Hải Dương: Người thân khóc ngất khi biết tin</a></h4>
                        <p>“Sang đến nơi thấy cảnh tượng như vậy tôi không còn đứng vững được nữa, đầu óc choáng váng và bị sốc....", bà Thể cho biết.</p>
                    </div>
           </div>
            <div id="itm-news5" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="CSGT lên tiếng về vụ dùng tay “va chạm” khiến người vi phạm đổ máu">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">CSGT lên tiếng về vụ dùng tay “va chạm” khiến người vi phạm đổ máu</a></h4>
                        <p>Sau khi đoạn clip được mạng xã hội chia sẻ ghi nhận cảnh nam thanh niên miệng chảy máu được lan truyền, Phòng CSGT đường bộ - đường sắt PC67, Công an TP.HCM đã lên tiếng.</p>
                    </div>
           </div>
            <div id="itm-news6" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="CHẤN ĐỘNG: Chồng đánh chết vợ, giấu xác suốt 4 năm ở hố ga trong nhà">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">CHẤN ĐỘNG: Chồng đánh chết vợ, giấu xác suốt 4 năm ở hố ga trong nhà</a></h4>
                        <p>Trong quá trình mâu thuẫn vì nghi vợ cặp bồ, Hoàng lấy khúc củi khô đánh chết vợ. Sau đó, người chồng đã giấu xác vợ suốt 4 năm ở hố ga trong nhà.</p>
                    </div>
           </div>
            <div id="itm-news7" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="Trung Quốc bắn 21 loạt đại bác chào mừng Chủ tịch nước Trần Đại Quang">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">Trung Quốc bắn 21 loạt đại bác chào mừng Chủ tịch nước Trần Đại Quang</a></h4>
                        <p>Chiều nay, lễ đón chính thức Chủ tịch nước Trần Đại Quang và phu nhân thăm cấp nhà nước tới TQ đã được tổ chức tại Đại lễ đường Nhân dân, Bắc Kinh theo nghi thức trọng thể dành đón nguyên thủ quốc gia.</p>
                    </div>
           </div>
            <div id="itm-news8" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="Xế hộp lấn làn phi thẳng xe khách trên cao tốc Hà Nội - Lào Cai">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">
                            Xế hộp lấn làn phi thẳng xe khách trên cao tốc Hà Nội - Lào Cai</a></h4>
                        <p>Vụ tai nạn xảy ra vào khoảng 15h15 chiều nay trên cao tốc Nội Bài - Lào Cai khiến nhiều người bị thương.</p>
                    </div>
            </div>
            <div id="itm-news9" class="news_item">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="images/sukiennoibat.png" alt="Bác sĩ cho thai phụ ngưng tim, cứu 2 mẹ con thoát chết">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">Bác sĩ cho thai phụ ngưng tim, cứu 2 mẹ con thoát chết</a></h4>
                        <p>Khi thai nhi được 12 tuần, thai phụ 20 tuổi người Bình Dương bất ngờ rơi vào tình trạng lơ mơ, mạch và huyết áp không ổn định, phải dùng thuốc hỗ trợ.</p>
                    </div>
            </div> 
        </div>
        
        <div class="ct_right">
            <ul class="news_lst">
                    <li>
                        <a onmouseover="Slidenews(0)" href="#">
                        Loạt tin nhắn gạ gẫm trai lạ nghi của Minh Béo khiến cư dân mạng dậy sóng</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(1)" href="#">
                        Mỹ Tâm trả lời về tin làm đám cưới vào 23/5</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(2)" href="#">
                        Ông Bùi Cao Nhật Quân từ chức Phó chủ tịch Novaland</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(3)" href="#">
                        Đến cả chuyện đi ăn với đồng nghiệp, mẹ chồng cũng quản thúc nàng dâu chặt chẽ</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(4)" href="#">
                        Vụ 3 anh em ruột tử vong ở Hải Dương: Người thân khóc ngất khi biết tin</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(5)" href="#">
                        CSGT lên tiếng về vụ dùng tay “va chạm” khiến người vi phạm đổ máu</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(6)" href="#">
                        CHẤN ĐỘNG: Chồng đánh chết vợ, giấu xác suốt 4 năm ở hố ga trong nhà</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(7)" href="#">
                        Trung Quốc bắn 21 loạt đại bác chào mừng Chủ tịch nước Trần Đại Quang</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(8)" href="#">
                        Xế hộp lấn làn phi thẳng xe khách trên cao tốc Hà Nội - Lào Cai</a>
                    </li>
                    <li>
                        <a onmouseover="Slidenews(9)" href="#">
                        Bác sĩ cho thai phụ ngưng tim, cứu 2 mẹ con thoát chết</a>
                    </li>

                    <div style="font-weight: bold; line-height: 35px; display:none">Tin doanh nghiệp</div>
                        <li style="display:none"><a href="#">
                        FE CREDIT ký kết hợp đồng hợp vốn 100 triệu USD với ngân hàng Credit Suisse</a></li>
            </ul>

        </div>
       
        <div class="clrb">
        </div>
    </div>

     <script language="javascript" type="text/javascript">
                var curPos1 = 0;
                var timer1 = setTimeout("Slidenews(0)", 9000);

                function Slidenews(index1) {
                    clearTimeout(timer1);

                    document.getElementById("itm-news" + curPos1).style.display = "none";
                    document.getElementById("itm-news" + index1).style.display = "";

                    curPos1 = index1;
                    index1 = (index1 == 9) ? 0 : (index1 + 1);
                    timer1 = setTimeout("Slidenews(" + index1 + ")", 9000);
                }
            </script>
    
    <div class="news_top_ct" id="news-top-ct-2" style="display: none;">
        <div class="ct_left">
            <div id="itm-news-hot0" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170507/clip-xe-tai-vu-qua-tram-thu-phi-truoc-vu-tai-nan-tham-khoc-29-07052017132814-400x300.jpg" alt="Clip xe tải vù qua trạm thu phí trước vụ tai nạn thảm khốc">
                    </div>
                </a>
                    <div class="text">
                        <h4>
                            <a href="#">
                            Clip xe tải vù qua trạm thu phí trước vụ tai nạn thảm khốc</a></h4>
                        <p> Khoảng 4h30 sáng 7-5, xe tải chở phân và xe khách giường nằm tông nhau tại đường Hồ Chí Minh (quốc lộ 14) thuộc địa bàn huyện Chư Sê, tỉnh Gia Lai làm 12 người chết và nhiều người bị thương. </p>
                    </div>
            </div>

            <div id="itm-news-hot1" class="news_item">                
                <a class="photo" href="#">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170508/nghi-pham-da-troi-be-gai-viet-5-tieng-lien-tuc-truoc-khi-sat-hai-35-08052017121231-400x300.jpg" alt="Nghi phạm đã trói bé gái Việt 5 tiếng liên tục trước khi sát hại">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Nghi phạm đã trói bé gái Việt 5 tiếng liên tục trước khi sát hại</a>
                    </h4>
                        <p>Theo thông tin từ NHK, cảnh sát cho biết có thể nghi phạm Shibuya đã trói tay và chân nạn nhân liên tục suốt 5 tiếng đồng hồ.</p>
                </div>
            </div>

            <div id="itm-news-hot2" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                        <div class="crop">
                            <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/chelsea-vo-dich-vi-premier-league-qua-te-25-11052017095723-400x300.jpg" alt="Chelsea vô địch vì Premier League quá tệ?">
                        </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Chelsea vô địch vì Premier League quá tệ?</a>
                    </h4>
                        <p>Chelsea đang tiến rất gần tới chức vô địch Premier League thứ 5 trong lịch sử sau chiến thắng trước Middlesbrough. Mặt khác, người ta vẫn băn khoăn sự thống trị của Chelsea mùa này nằm ở nội tại hay là chất lượng Premier League đang đi xuống?</p>
                </div>
            </div>

            <div id="itm-news-hot3" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/ronaldo-lai-nhon-chan-an-gian-chieu-cao-25-11052017140257-400x300.jpg" alt="Ronaldo lạinhón chân, &quot;ăn gian&quot; chiều cao">
                    </div>
                </a>

                <div class="text">
                        <h4>
                            <a href="#">Ronaldo lại nhón chân, "ăn gian" chiều cao</a></h4>
                        <p>Cristiano Ronaldo luôn "tỉnh táo" để có thể trông đẹp nhất ở mọi bức ảnh, dù đang trong tâm trạng phấn khích vì vào chung kết Champions League.</p>
                </div>
            </div>

            <div id="itm-news-hot4" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170503/ong-le-dinh-kinh-khong-ai-ep-duoc-nguoi-dung-dau-ha-noi-29-03052017183829-400x300.jpg" alt="Ông Lê Đình Kình: Không ai ép được người đứng đầu Hà Nội">
                    </div>
                </a>
                
                <div class="text">
                    <h4>
                        <a href="#">Ông Lê Đình Kình: "Không ai ép được người đứng đầu Hà Nội"</a>
                    </h4>
                        <p>Nói về bản cam kết giữa lãnh đạo Hà Nội với người dân xã Đồng Tâm, ông Lê Đình Kình cho rằng đó là một quyết định vì đại cuộc bởi "không ai ép được người đứng đầu thành phố" làm như vậy.</p>
                </div>
            </div>

            <div id="itm-news-hot5" class="news_item" style="display: none;">                
                <a class="photo" href="#">
                    <div class="crop">
                        <img width="400" src="http://media.docbao.vn/\files\images\site-1\20170511/dong-doi-pha-hong-man-ra-mat-hoan-hao-cua-xuan-truong-25-11052017211542-400x300.jpg" alt="Đồng đội phá hỏng màn ra mắt hoàn hảo của Xuân Trường">
                    </div>
                </a>
                    
                <div class="text">
                    <h4>
                        <a href="#">Đồng đội phá hỏng màn ra mắt hoàn hảo của Xuân Trường</a></h4>
                    <p>Dù lập siêu phẩm và có đóng góp lớn trong lối chơi của Gangwon, Xuân Trường không thể giúp đội nhà chiến thắng vì một tấm thẻ đỏ đáng tiếc..</p>
                </div>
            </div>        

        </div>
        
        <div class="ct_right">
            <ul class="news_lst">
                <li><a onmouseover="Slidenewshot(0)" href="#"><strong>Tai nạn thảm khốc ở Gia Lai, 12 người chết</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(0)" style="font-weight: normal;">Clip xe tải vù qua trạm thu phí trước vụ tai nạn thảm khốc</a></li>
                <li><a onmouseover="Slidenewshot(1)" href="#"><strong>Bé gái Việt bị sát hại ở Nhật Bản</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(1)" style="font-weight: normal;">Nghi phạm đã trói bé gái Việt 5 tiếng liên tục trước khi sát hại</a></li>
                <li><a onmouseover="Slidenewshot(2)" href="#"><strong>Bóng đá Ngoại Hạng Anh (Premier League)</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(2)" style="font-weight: normal;">Chelsea vô địch vì Premier League quá tệ?</a></li>
                <li><a onmouseover="Slidenewshot(3)" href="#"><strong>Thể thao quốc tế</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(3)" style="font-weight: normal;">Ronaldo lại nhón chân, "ăn gian" chiều cao</a></li>
                <li><a onmouseover="Slidenewshot(4)" href="#"><strong>Người dân gây rối trật tự ở Đồng Tâm - Mỹ Đức </strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(4)" style="font-weight: normal;">Ông Lê Đình Kình: "Không ai ép được người đứng đầu Hà Nội"</a></li>
                <li><a onmouseover="Slidenewshot(5)" href="#"><strong>Thể thao trong nước</strong></a><br>
                    <a href="#5" onmouseover="Slidenewshot(5)" style="font-weight: normal;">Đồng đội phá hỏng màn ra mắt hoàn hảo của Xuân Trường</a></li>
            </ul>
        </div>
        <div class="clrb">
        </div>
    </div>

    <script language="javascript" type="text/javascript">
            var curPos2 = 0;
            var timer2 = setTimeout("Slidenewshot(1)", 9000);

            function Slidenewshot(index2) {
                clearTimeout(timer2);

                document.getElementById("itm-news-hot" + curPos2).style.display = "none";
                document.getElementById("itm-news-hot" + index2).style.display = "";

                curPos2 = index2;
                index2 = (index2 == 5) ? 0 : (index2 + 1);
                timer2 = setTimeout("Slidenewshot(" + index2 + ")", 9000);
            }
        </script>



</div>
        