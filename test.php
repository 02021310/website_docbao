<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Đọc Báo - Website Tin tức Điện tử - Báo điện tử - Tin nhanh - Tin mới hàng ngày - Tin tức 24h | DocBao.Vn</title>


    <!-- CSS -->
    <link type="text/css" rel="stylesheet" href="code/reset.css" />
    <link type="text/css" rel="stylesheet" href="code/docbao.css" />
    <!-- JAVASCRIPT -->
   


<body >
    <form id="form1">

        <?php require "nav_header_menu.php" ?>

        <div class="wrapper">


            <div class="header">
                <div class="logozone">     
                    <a alt='Doc bao' href="/" class="logo">  
                      <img alt="Logo docbao.vn" width="250px" src="images/home_logo_header1.jpg" />
                  </a>
                  <div class="advbox">    
                    <div class="ad" style="float: left">        
                        <img src="images/home_logo_header1_2.jpg">                    
                    </div>
                </div>
                <div class="clrb">
                </div>
            </div>




            <ul class="menubar">
                <li><a href="#" class="home on"><span></span></a></li>
                <li><a href="#">Xã hội</a></li>
                <li><a href="#">Thế giới</a></li>
                <li><a href="#">Pháp luật</a></li>
                <li><a href="#">Kinh tế</a></li>
                <li><a href="#">Sao 360°</a></li>
                <li><a href="#">Giải trí</a></li>
                <li><a href="#">Thể thao</a></li>
                <li><a href="#">Video</a></li>       
                <li><a href="#">Giới trẻ</a></li>
                <li><a href="#">Gia đình</a></li>
                <li><a href="#">Giới tính</a></li>
                <li><a href="#">Quân Sự</a></li>
                <li><a href="#">Ôtô-Xe máy</a></li>
                <li><a href="#">Đời sống</a></li>
                <li><a href="#">Hi-tech</a></li>
                <li><a href="#">Doanh nghiệp</a></li>
                <div class="clrb">
                </div>
            </ul>
        </div>

        <div id="abdMastheadMb" style="margin:0 auto; width:100%;">
        </div>

        <div class="advfullwidth">  
            <img src="images/home_logo_header2.png">
        </div>

      


        <div class="layout2col">
            <div class="col_left">        

                <div>
                    <?php require "sukiennoibat.php" ?>
                </div>   

                <script type="text/javascript">
                    $("#news-top-tabs-1").click(function () {
                        $(this).addClass("on");
                        $("#news-top-tabs-2").removeClass("on");
                        $("#news-top-ct-2").hide();
                        $("#news-top-ct-1").show();
                    });

                    $("#news-top-tabs-2").click(function () {
                        $(this).addClass("on");
                        $("#news-top-tabs-1").removeClass("on");
                        $("#news-top-ct-1").hide();
                        $("#news-top-ct-2").show();
                    });
                </script>
                
                <br clear="all">

                <div>
                    <?php require "depplus.php" ?>
                </div>

                <div>
                    <!-- <?php require "quangcaiadmiac.php" ?> -->
                </div>

                <br clear="all">

                <div>
                    <?php require "xahoithegioi.php" ?>
                </div>
                <br clear="all">                

                <style>
                    .bulli {
                        background: url('images/icon_list.gif') no-repeat 0 5px transparent;
                        padding-left: 12px;
                        margin-bottom: 5px;
                        margin-left: 5px;
                        width: 46% !important;
                    }

                    .jcarousel {
                        position: relative;
                        overflow: hidden;
                    }

                    .jcarousel ul {
                        width: 20000em;
                        position: relative;
                        list-style: none;
                        margin: 0;
                        padding: 0;
                    }

                    .jcarousel li {
                        float: left;
                    }

                    .clip .bar, .docnhieunhat .bar {
                        border-top: solid 2px #ff9300;
                        text-align: left;
                        font: normal 16px/1.3 'Myriad';
                        color: #171717;
                        padding: 6px 0;
                    }

                    .clip li {
                        width: 156px;
                        overflow: hidden;
                        margin-right: 20px;
                    }

                    .clip li a {
                        display: block;
                        float: left;
                        position: relative;
                    }

                    .clip li img {
                        display: block;
                        float: left;
                    }

                    .clip li h3 {
                        font: bold 12px/1.3 arial;
                        color: Black;
                        overflow: hidden;
                        background-color: #ededed;
                        padding: 6px;
                        height: 55px;
                    }

                    .clip li a .video_ico {
                        top: 45px;
                        left: 0;
                    }

                    .clip li a:hover .video_ico {
                        background-position: -309px 0;
                    }

                    .clip .slide_clip {
                        height: 185px;
                    }

                    .truyenhinh li h3 {
                        height: 60px;
                    }

                    .nguoidep li h3, .sukien_anh li h3 {
                        height: 32px;
                    }

                    .clip .slide_clip_page, .truyenhinh .slide_truyenhinh_page, .sukien_anh .slide_sukien_anh_page, .nguoidep .slide_nguoidep_page {
                        float: right;
                    }

                    .clip .slide_clip_page a, .truyenhinh .slide_truyenhinh_page a, .sukien_anh .slide_sukien_anh_page a, .nguoidep .slide_nguoidep_page a {
                        display: inline-block;
                        width: 8px;
                        height: 8px;
                        background-color: #999;
                        border-radius: 5px;
                        margin-left: 5px;
                        text-indent: -9999px;
                        line-height: 7px;
                    }

                    .clip .slide_clip_page a.active, .clip .slide_clip_page a:hover, .truyenhinh .slide_truyenhinh_page a.active, .truyenhinh .slide_truyenhinh_page a:hover, .sukien_anh .slide_sukien_anh_page a.active, .sukien_anh .slide_sukien_anh_page a:hover, .nguoidep .slide_nguoidep_page a.active, .nguoidep .slide_nguoidep_page a:hover {
                        background-color: #be0000;
                    }

                    .sprite {
                        background: url(/images/sprite_19_03_2015.png) no-repeat 0 0;
                    }

                    .video_ico {
                        width: 44px;
                        height: 25px;
                        background-position: -260px 0;
                        position: absolute;
                    }
                </style>

                <div>
                    <?php require "video_anh_audio.php" ?>
                </div>

                <br clear="all">

                <div>
                    <?php require "phapluat.php" ?>            
                </div>

                <br clear="all">          

                <div>
                    <?php require "gioitre.php" ?>
                </div>

                <br clear="all">            

                <div>
                    <?php require "Sao.php" ?>
                </div>

                <br clear="all">

                <div>
                    <?php require "giaitri.php" ?>
                </div>

                <br clear="all">
                <div>
                    <?php require "thethao.php" ?>
                </div>          

                <br clear="all">

                <div>
                    <?php require "doisong_suckhoe.php" ?>
                </div>

                <br clear="all">

                <div>
                    <?php require "quansu_chuyenla_dep.php" ?>
                </div>

                <div>
                    <?php require "kinhte_oto_xemay.php" ?>
                </div>

                <br clear="all">
                
                <div>
                    <?php require "giadinh.php" ?>
                </div>

                <br clear="all">

                <div>
                    <?php require "hi_tech.php" ?>
                </div>

                </div>

                <?php   "quangcaobenphai.php" ?>
        </div>
    </div>

    <div align="center" style="clear: both;">
    </div>
</div>



<div class="footer">
    <div class="footer_nav">
        <div class="footer_nav_ct">
            <a href="/" class="home"><span></span></a>
            <a href="/tin-tuc/xa-hoi/29/">Xã hội</a>
            <a href="/tin-tuc/the-gioi/35/">Thế giới</a>
            <a href="/tin-tuc/phap-luat/33/">Pháp luật</a>
            <a href="/tin-tuc/kinh-te/30/">Kinh tế</a>
            <a href="/tin-tuc/sao-360/20/">Sao 360°</a>
            <a href="/tin-tuc/giai-tri/31/">Giải trí</a>
            <a href="/tin-tuc/the-thao/25/">Thể thao</a>
            <a href="/tin-tuc/euro-2016/73/">Euro 2016</a>
            <a href="/tin-tuc/gioi-tre/10/">Giới trẻ</a>
            <a href="/tin-tuc/gia-dinh/14/">Gia đình</a>
            <a href="/tin-tuc/gioi-tinh/16/">Giới tính</a>
            <a href="/tin-tuc/chuyen-la/18/">Chuyện lạ</a>
            <a href="/tin-tuc/dep/38/">Đẹp</a>
            <a href="/tin-tuc/doi-song/13/">Đời sống</a>
            <a href="/tin-tuc/cong-nghe/32/">Hi-tech</a>
            <a href="/tin-tuc/doanh-nghiep/65/">Doanh nghiệp</a>
        </div>
    </div>
    <div class="footer_ct">
        <a href="/" class="logo">
            <img alt="Logo docbao.vn" width="250px" src="images/logo_footer.jpg" />
        </a>
        <div class="info">
            <p>
                Trang tin tức điện tử của : docbao.vn
            </p>
            <p>
                Công ty Cổ phần Abcxyz         
            </p>
            <p>
                Giấy phép...
            </p>
            <p>Vận hành bởi tổ chức abc</p>
            <p>Tòa nhà C1 175 Tây Sơn Đông Đa Hà Nội</p>
            <p>Chịu trách nhiệm nội dung: Nguyễn Văn A</p>
            <p style="margin-top: 10px;">
                Liên hệ <a href="#">Quảng cáo</a> | <a href="#">Thông tin
            </a>
        </p>
    </div>

</div>
</div>

</form>
<a style="display: none" class="go_top" onclick="jQuery('html,body').animate({scrollTop: 0},500);"
title="Lên đầu trang" href="javascript:;"></a>



</body>
</html>
