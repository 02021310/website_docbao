<div class="news_box_type4">
    <h4 class="title">
        <a href="/tin-tuc/the-thao/25">Thể Thao</a></h4>
    <div class="main_ct">
        <div class="m_left">
                <a href="/tin-tuc/11-05-2017/bao-italy-ronaldo-nen-coi-chung-buffon/25/452058">
                    <img width="340" height="255" src="images/thethao.png" alt="Báo Italy: Ronaldo nên coi chừng Buffon"></a>
                <div class="text">
                    <h5>
                        <a href="/tin-tuc/11-05-2017/bao-italy-ronaldo-nen-coi-chung-buffon/25/452058">Báo Italy: Ronaldo nên coi chừng Buffon</a></h5>
                    <p>Truyền thông Italy cho rằng sức mạnh của Ronaldo và các đồng đội không hề hấn gì trước Juventus khi trận chung kết Champions League diễn ra tại Cardiff vào tháng tới.</p>
                </div>
        </div>
        <div class="m_right">
            <ul>
                    <li><a href="/tin-tuc/11-05-2017/vo-si-viet-nam-lan-thu-6-vo-dich-muay-thai-the-gioi/25/452057">
                        <img width="165" height="124" src="images/thethao.png" alt="Võ sĩ Việt Nam lần thứ 6 vô địch Muay Thái thế giới"></a>
                        <h6>
                            <a href="/tin-tuc/11-05-2017/vo-si-viet-nam-lan-thu-6-vo-dich-muay-thai-the-gioi/25/452057">Võ sĩ Việt Nam lần thứ 6 vô địch Muay Thái thế giới</a></h6>
                    </li>
                    <li><a href="/tin-tuc/11-05-2017/vo-si-mma-ha-guc-mon-do-vinh-xuan-quyen-trong-20-giay/25/452049">
                        <img width="165" height="124" src="images/thethao.png" alt="Võ sĩ MMA hạ gục môn đồ Vịnh Xuân Quyền trong 20 giây"></a>
                        <h6>
                            <a href="/tin-tuc/11-05-2017/vo-si-mma-ha-guc-mon-do-vinh-xuan-quyen-trong-20-giay/25/452049">Võ sĩ MMA hạ gục môn đồ Vịnh Xuân Quyền trong 20 giây</a></h6>
                    </li>
                    <li><a href="/tin-tuc/11-05-2017/dong-doi-pha-hong-man-ra-mat-hoan-hao-cua-xuan-truong/25/452045">
                        <img width="165" height="124" src="images/thethao.png" alt="Đồng đội phá hỏng màn ra mắt hoàn hảo của Xuân Trường"></a>
                        <h6>
                            <a href="/tin-tuc/11-05-2017/dong-doi-pha-hong-man-ra-mat-hoan-hao-cua-xuan-truong/25/452045">Đồng đội phá hỏng màn ra mắt hoàn hảo của Xuân Trường</a></h6>
                    </li>
                    <li><a href="/tin-tuc/11-05-2017/mourinho-kich-no-4-bom-tan-conte-duoc-thuong-dam/25/452032">
                        <img width="165" height="124" src="images/thethao.png" alt="Mourinho &quot;kích nổ&quot; 4 bom tấn, Conte được thưởng đậm"></a>
                        <h6>
                            <a href="/tin-tuc/11-05-2017/mourinho-kich-no-4-bom-tan-conte-duoc-thuong-dam/25/452032">Mourinho "kích nổ" 4 bom tấn, Conte được thưởng đậm</a></h6>
                    </li>
            </ul>
            <div class="clrb">
            </div>
        </div>
        <div class="clrb">
        </div>
    </div>
</div>